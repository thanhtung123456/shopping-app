import { Test } from '@nestjs/testing';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { TopProduct } from './topproduct.model';

@Injectable()
export class TopProductsService {
    constructor(
        @InjectModel('TopProduct') private readonly productModel: Model<TopProduct>,
    ) { }

    async insertProduct(
        name: string,
        brand: string,
        descProduct: string,
        OldPrice: number,
        NewPrice: number,
        ColorProduct: object,
        ImgUrlProduct: object,
        RateProduct: object[],
        QuantityProductAndSize: object,
        Categories: string[],
        commentId: string,
    ) {
        const newProduct = new this.productModel({
            name: name,
            brand: brand,
            descProduct: descProduct,
            OldPrice: OldPrice,
            NewPrice: NewPrice,
            ColorProduct: ColorProduct,
            ImgUrlProduct: ImgUrlProduct,
            RateProduct: RateProduct,
            QuantityProductAndSize: QuantityProductAndSize,
            Categories: Categories,
            commentId: commentId,
        });
        const result = await newProduct.save();
        return result.id as string;
    }

    async getProducts() {
        const products = await this.productModel.find().exec();
        return products.map(prod => ({
            _id: prod._id,
            name: prod.name,
            brand: prod.brand,
            descProduct: prod.descProduct,
            OldPrice: prod.OldPrice,
            NewPrice: prod.NewPrice,
            ColorProduct: prod.ColorProduct,
            ImgUrlProduct: prod.ImgUrlProduct,
            RateProduct: prod.RateProduct,
            QuantityProductAndSize: prod.QuantityProductAndSize,
            Categories: prod.Categories,
            commentId: prod.commentId,
        }));
    }

    async getSingleProduct(productId: string) {
        const product = await this.findProduct(productId);
        return {
            _id: product._id,
            name: product.name,
            brand: product.brand,
            descProduct: product.descProduct,
            OldPrice: product.OldPrice,
            NewPrice: product.NewPrice,
            ColorProduct: product.ColorProduct,
            ImgUrlProduct: product.ImgUrlProduct,
            RateProduct: product.RateProduct,
            QuantityProductAndSize: product.QuantityProductAndSize,
            Categories: product.Categories,
            commentId: product.commentId,
        };
    }

    async updateProduct(
        productId: string,
        name?: string,
        brand?: string,
        descProduct?: string,
        OldPrice?: number,
        NewPrice?: number,
        ColorProductUpdate?: string[],
        ImgUrlProduct?: object,
        RateProduct?: object[],
        QuantityProductAndSize?: any[],
        Categories?: string[],
        commentId?: string,
    ) {
        const product = await this.findProduct(productId);
        const ColorProduct = product.ColorProduct;
        const CategoriesData = product.Categories;
        const updatedProduct = await this.findProduct(productId);
        if (name) {
            updatedProduct.name = name;
        }
        if (brand) {
            updatedProduct.brand = brand;
        }
        if (descProduct) {
            updatedProduct.descProduct = descProduct;
        }
        if (OldPrice) {
            updatedProduct.OldPrice = OldPrice;
        }
        if (NewPrice) {
            updatedProduct.NewPrice = NewPrice;
        }
        if (ColorProductUpdate) {
            const AssignColor = [...ColorProduct, ...ColorProductUpdate];
            const UpdateDataColor = Array.from(new Set(AssignColor));
            // console.log(UpdateDataColor);
            updatedProduct.ColorProduct = UpdateDataColor;
        }

        if (Categories) {
            const AssignCategories = [...CategoriesData, ...Categories];
            const UpdateDataCategories = Array.from(new Set(AssignCategories));
            // console.log(UpdateDataColor);
            updatedProduct.ColorProduct = UpdateDataCategories;
        }

        if (commentId) {
            updatedProduct.commentId = commentId;
        }

        if (ImgUrlProduct) {

            const updateImgUrlProduct = { ...ImgUrlProduct }
            updatedProduct.ImgUrlProduct = updateImgUrlProduct;
        }
        if (RateProduct) {
            updatedProduct.RateProduct = Object.assign(RateProduct);
        }
        if (QuantityProductAndSize) {
            updatedProduct.QuantityProductAndSize = QuantityProductAndSize;
        }
        updatedProduct.save();
    }

    async deleteProduct(prodId: string) {
        const result = await this.productModel.deleteOne({ _id: prodId }).exec();
        if (!result) {
            throw new NotFoundException('Could not find product.');
        }
    }

    private async findProduct(id: string): Promise<TopProduct> {
        let product;
        try {
            product = await this.productModel.findById(id).exec();
        } catch (error) {
            throw new NotFoundException('Could not find product.');
        }
        if (!product) {
            throw new NotFoundException('Could not find product.');
        }
        return product;
    }
}
