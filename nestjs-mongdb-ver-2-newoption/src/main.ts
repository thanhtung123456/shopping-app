import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import * as admin from 'firebase-admin';
// import {getStorage } from 'firebase-admin/storage';

import { AppModule } from './app.module';
// import 'dotenv/config'
import * as config from 'config';
import * as express from 'express';
const mongoose = require("mongoose");


async function bootstrap() {

  // set up cors cho server tranhs truong hop truy cap yeu cau Cors

  const app = await NestFactory.create(AppModule);

  //Firebase Connect
  if (!admin.apps.length) {
    admin.initializeApp({
      credential: admin.credential.cert({
        clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
        privateKey: process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '/n'),
        projectId: process.env.FIREBASE_PROJECT_ID
      } as Partial<admin.ServiceAccount>),
      databaseURL: process.env.FIREBASE_DATABASE_URL,
      storageBucket: process.env.FIREBASE_STORAGE_BUCK
    })
    console.log(admin.SDK_VERSION, "app")
    // const bucket = getStorage().bucket();
  }


  // console.log(process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '/n'))

  // app.enableCors(
  //   {
  //     origin: ['http://localhost:3000', 'http://localhost:9000', "*"],
  //     methods: "GET,HEAD,POST,PUT,DELETE,PATCH",
  //     allowedHeaders: "Content-Type, Accept",
  //     credentials: true,
  //   }
  // );
  // const serverConfig = config.get("server")
  app.useGlobalPipes(new ValidationPipe());

  if (process.env.NODE_ENV === "development") {

    app.enableCors(
      {
        origin: "*",
        methods: "GET,HEAD,POST,PUT,DELETE,PATCH",
        allowedHeaders: "Content-Type, Accept",
        credentials: true,
      }
    );
  } else {

    // app.enableCors({
    //   origin: "*",
    //   methods: "GET,HEAD,POST,PUT,DELETE,PATCH",
    //   allowedHeaders: "Content-Type, Accept",
    // credentials: true,
    // })
    app.enableCors({ origin: process.env.Origin });
    // console.log("Accepting Orgin: ");
  }

  const PORT = process.env.PORT || 9000

  app.use(express.json());


  mongoose
    .connect("mongodb+srv://ShoppingApp:ShoppingApp@cluster0.2bryy.mongodb.net/ShoppingApp?retryWrites=true&w=majority", {
      // autoCreate: true
      // useUnifiedTopology: true,
      // useCreateIndex: true,
    })
    .then(() => console.log("DB Connection Successful"))
    .catch((err) => {
      console.error(err);
    });

  await app.listen(PORT, () => {
    console.log("Start Listening with Port:", PORT);
  });
}
export default admin;
bootstrap();
