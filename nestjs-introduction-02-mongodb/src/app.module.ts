import { TopProductsModule } from './topproducts/topproducts.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { UserModule } from './users/user.module'
import { GraphQLModule } from '@nestjs/graphql';
import { ConfigModule } from '@nestjs/config'
import 'dotenv/config'


const URL = process.env.DATABASE_URL
// console.log(URL);

@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      debug: false,
      autoSchemaFile: true,

    }),
    ProductsModule,
    TopProductsModule,
    UserModule,
    MongooseModule.forRoot(URL, {
      autoCreate: true
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
