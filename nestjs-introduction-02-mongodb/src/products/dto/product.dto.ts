import { Field, Int, ObjectType } from '@nestjs/graphql';



@ObjectType()
export class ImgProduct {
    @Field()
    imgfirst?: string;

    @Field()
    imgsecond?: string;

    @Field()
    imgthird?: string;
}

@ObjectType()
export class Rate {
    @Field(() => [Number!])
    rate?: Number[];
}

@ObjectType()
export class Quantity {
    @Field()
    size?: string;

    @Field()
    QuantityProduct?: number;
}


@ObjectType()
export class ProductDto {
    @Field()
    readonly id?: string;

    @Field()
    readonly name?: string;

    @Field()
    readonly brand?: string;

    @Field()
    readonly descProduct?: string;

    @Field(() => Int)
    readonly OldPrice?: number;

    @Field(() => Int)
    readonly NewPrice?: number;

    @Field(type => [String!])
    readonly ColorProduct?: string[];

    @Field()
    ImgUrlProduct?: ImgProduct;

    @Field()
    readonly DetailProduct?: Rate;

    @Field(() => [Quantity])
    readonly QuantityProductAndSize?: Quantity[];

}
